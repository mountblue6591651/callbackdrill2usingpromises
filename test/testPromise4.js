const searchByName = require('../promise4.js');
const cardInfo = require('../promise3.js');
const listInfo = require('../promise2.js');
const boardInfo = require('../promise1.js');


let name = "Thanos";

searchByName(name)
.then((data) => {
    boardInfo(data)
    .then((res) => {
        console.log(res);
    })
    .catch((err) => {
        console.error(err);
    });
    return listInfo(data);
})
.then((data) => {
    console.log(data);
    let cardId = data.filter((card) => {
        if(card.name === 'Mind'){
            return true;
        }
    }).map((card) => card.id);
    return cardInfo(cardId);
})
.then((data) => {
    console.log(data);
})
.catch((error) => {
    console.error(error);
})
