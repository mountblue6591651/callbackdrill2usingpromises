// Import the boardInfo function from the 'promise1.js' module
const boardInfo = require("../promise1.js");

// Specify the ID to search for
let id = "mcu453ed";

// Call the boardInfo function with the specified ID
boardInfo(id)
  .then((data) => {
    // Log the retrieved data when the promise is resolved
    console.log(data);
  })
  .catch((error) => {
    // Log any errors that occur during the process
    console.error(error);
  });
