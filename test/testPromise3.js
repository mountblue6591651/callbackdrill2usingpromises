// Import the cardInfo function from the promise3.cjs file
const cardInfo = require('../promise3.js');

// Specify the ID to search for
let id = 'qwsa221';

// Call the cardInfo function with the specified ID 
cardInfo(id)
.then((data) => {
    console.log(data);
})
.catch((error) => {
    console.error(error);
})
