const fs = require('fs');
const { promises } = require("fs");

function read(path) {
    return promises.readFile(path, "utf-8");
}

const searchByName = function(name) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            read('./boards_1.json')
            .then((data) => {
                let boards = JSON.parse(data);
                let id = boards.filter((board) => {
                    return board.name === name;
                }).map((board) => board.id);
                
                if (id.length) {
                    resolve(id[0]);   
                }
            })
            .catch((error) => {
                reject(error);
            })
        }, 2500); 
    })
    
}

module.exports = searchByName;
