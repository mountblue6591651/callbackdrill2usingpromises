const fs = require("fs");
const { promises } = require("fs");

// Function to read a file asynchronously
function read(path) {
  return promises.readFile(path, "utf-8");
}

// Function to retrieve board information by ID with a delay of 2 seconds
const listInfo = function (id) {
  return new Promise((resolve, reject) => {
    // Simulate a delay using setTimeout
    setTimeout(() => {
      // Read the content of the "lists_1.json" file
      read("./lists_1.json")
        .then((data) => {
          // Parse the JSON data from the file
          let lists = JSON.parse(data);
        
          if(lists[id]){
            resolve(lists[id]);
          }
          else{
            reject('No data found');
          }
        })
        .catch((err) => {
          // If there is an error reading the file, reject the promise
          reject(err);
        });
    }, 2000); // Wait for 2000 milliseconds (2 seconds)
  });
};

// Export the boardInfo function for external use
module.exports = listInfo;
