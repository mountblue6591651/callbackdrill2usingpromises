const fs = require("fs");
const { promises } = require("fs");

function read(path) {
  return promises.readFile(path, "utf-8");
}

const boardInfo = function (id) {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      read("./boards_1.json")
        .then((data) => {
          let boards = JSON.parse(data);

          let info = boards.filter((board) => {
            return board.id === id;
          });

          if (info.length) {
            resolve(info);
          }
        })
        .catch((err) => {
          // If there is an error reading the file, reject the promise
          reject(err);
        });
    }, 2000); // Wait for 2000 milliseconds (2 seconds)
  });
};

// Export the boardInfo function for external use
module.exports = boardInfo;
